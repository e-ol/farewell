# Perfidy

[![License](https://img.shields.io/badge/License-MPL_2.0-red.svg?style=flat-square)](https://gitlab.com/loopingleaf/perfidy/blob/master/LICENSE)
[![Discord](https://img.shields.io/discord/385001237797666827.svg?label=Discord&logo=discord&logoColor=white&style=flat-square)](https://discord.gg/nqyKQ9Z)
[![Twitter](https://img.shields.io/twitter/url?label=Follow&style=social&url=https%3A%2F%2Ftwitter.com%2Floopingleaf)](https://twitter.com/loopingleaf)

Perfidy is a multiplayer video game project developed on Ruby with the 
Ruby Gem [Gosu](https://www.libgosu.org/).

The action take place in a heroic fantasy inn, where you with the other
occupants, are trapped together by monstrous beasts. Your goal is to survive day
after day the assaults of the beasts.\
However, you never know who you can trust. In this world, it is rare not to be
bound by an oath, a temptation or a desease that forces you to act against your
stooges.

## Installation

*TODO*
```shell
apt-get install ruby # or pacman -Syy ruby
gem install bundler
git clone https://gitlab.com/e-ol/deadlocked.git
cd deadlocked
bundle
ruby main.rb
```

## Farewell lib

Gosu is a pretty lightwheight game engine. Farewell is the lib I have created
and I use on top of Gosu. It wrap the `Gosu::Image` instances into a convinient
`Farewell::Entity` class, handles events and states (which is more or less like
a scene in _Unity_), integrates a tcp client and a playlist for the songs.
_It doesn't make cookies though._

It is licenced under the MPL 2.0, so feel free to use, but keep in mind that
I've done this lib for myself; the logic and some choices may not suit you.
To add the lib to your project, just copy/paste the whole `lib/farewell` dir
somewhere inside your project, then in your `main.rb` (or `app.rb`), include
the `farewell/main.rb` file and you're good to go.

_Farewell firstly was the name of this whole game project, before becoming
Perfidy_

## About

Copyright 2017-2019 (c) Luc Lauriou  
All the Ruby sources files of this project are licensed under the MPLv2. All
changes to these files automatically become MPLv2 as well. See 
[LICENSE](LICENSE) for more information. Not every files of this project are
licensed under MPLv2, see below for more information about specific file/library
and its license, if any.

### Libraries / Tools

-   [Gosu](https://www.libgosu.org/) and all documentation (c) 2001-2020 Julian Raschke, Jan Lücker, and other contributors -
[![Licence: MIT](https://img.shields.io/badge/License-MIT-yellow.svg?style=flat-square)](https://github.com/gosu/gosu/blob/master/COPYING)

### Music

-   [The Dance Of Death](https://opengameart.org/content/the-dance-of-death)
by  Michael Klier (menu theme) -
[![License: CC BY-SA 3.0](https://licensebuttons.net/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by-sa/3.0/)
-   [Larik](https://opengameart.org/content/larik) by [Gundatsch](https://soundcloud.com/gundatsch) - [![License: CC BY](https://licensebuttons.net/l/by-sa/4.0/80x15.png)](https://creativecommons.org/licenses/by/3.0/),
[![License: OGA-BY 3.0](https://img.shields.io/badge/License-OGA--BY%203.0-lightgrey?style=flat-square)](http://static.opengameart.org/OGA-BY-3.0.txt)

### Images

-   [Keyboard/Controller Keys](https://hyohnoo.itch.io/keyboard-controller-keys)
by Hyohnoo -
[![License: CC0-1.0](https://licensebuttons.net/p/zero/1.0/80x15.png)](https://creativecommons.org/publicdomain/zero/1.0/deed.fr)

### Fonts

-   [BitPotion](https://joebrogers.itch.io/bitpotion)
by Joeb Rogers -
[![License: CC BY 4.0](https://licensebuttons.net/l/by/4.0/80x15.png)](https://creativecommons.org/licenses/by/4.0/)
-   [BluuNext](https://github.com/jbmorizot/BluuNext/blob/master/OFL_BluuNext.txt) -
Copyright (c) 2014, Jean-Baptiste Morizot, (http://cargocollective.com/jbmrz),
with Reserved Font Name "BluuNext".\
Copyright (c) <2014>, Velvetyne.fr (http://velvetyne.fr/) -
[![Licence: OTF](https://scripts.sil.org/cms/sites/nrsi/media/OFL_logo_rect_color.png)](https://github.com/jbmorizot/BluuNext/blob/master/OFL_BluuNext.txt)
