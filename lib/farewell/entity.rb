# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

module Farewell
  # An actor in the state, displayed on the screen, with a hitbox and all.
  # Equivalent to GameObject in Unity.
  #
  # @todo +v_align+ & +h_align+ methods.
  class Entity
    attr_reader :width, :height, :x, :y, :z, :hitbox, :events, :drawable

    # Entity initializer
    #
    # @param drawable [Gosu::Image, Array<Gosu::Image>] a drawable object that
    #   is displayed on #draw calls.
    # @param x [Integer] the x coordinate of the object on the game window.
    # @param y [Integer] the y coordinate of the object on the game window.
    # @param z [Integer] the layer of the object on the game window.
    # @param width [Integer, nil] the width of the entity. If `nil`, take the
    #   drawable's width instead.
    # @param height [Integer, nil] the height of the entity. If `nil`, take the
    #   drawable's height instead.
    # @param hitbox [Integer] set the hitbox for #hover?, #clicked? event. Is
    #   added to the width an height.
    # @param v_align [Symbol, nil] align the entity. Can be `:center`,
    #   `:bottom`, or `nil`.
    # @param h_align [Symbol, nil] align the entity. Can be `:center`, `:right`,
    #   or `nil`.
    def initialize(drawable, x = 0, y = 0, z = 0, width: nil, height: nil,
                   tag: nil, hitbox: 0, v_align: nil, h_align: nil)
      @drawable = drawable
      @width =
        if width.nil?
          @drawable.class == Array ? @drawable[0].width : @drawable.width
        else
          width
        end
      @height =
        if height.nil?
          @drawable.class == Array ? @drawable[0].height : @drawable.height
        else
          height
        end

      # TODO: centering is now natively implemented in Gosu, h_align and v_align
      #   need to be changed
      if h_align.nil?
        @x = x if x.is_a?(Integer)
      elsif h_align == :center
        @x = Window.instance.width / 2
        @x -= @width / 2
        @x += x if x.is_a?(Integer)
      elsif h_align == :right
        @x = Window.instance.width
        @x -= @width
        @x += x if x.is_a?(Integer)
      end
      if v_align.nil?
        @y = y if y.is_a?(Integer)
      elsif v_align == :center
        @y = Window.instance.height / 2
        @y -= @height / 2
        @y += y if y.is_a?(Integer)
      elsif v_align == :bottom
        @y = Window.instance.height
        @y -= @height
        @y += y if y.is_a?(Integer)
      end
      @z = z
      @tag = tag
      @hitbox = hitbox
      @color = Gosu::Color::WHITE # TODO: Remove that
      @events = []
      Farewell::State.active_state.add_entity(self)
    end

    class << self
      # not implemented
      def find_by_coordinates(x, y, scope: :state)
        if scope == :state

        elsif scope == :all

        end
      end

      def f_xy(x, y, scope: :state)
        find_by_coordinates(x, y, scope)
      end

      def hover_list
        hover_list = []
        Farewell::State.active_state.entity_list.each do |entity|
          hover_list.push(entity) if entity.hover?
        end
        hover_list
      end
    end

    # Draw the drawable on the screen
    # @param color [Gosu::Color] The color in which to draw the drawable
    # @param index [Integer | Nil] The index of the item to draw if the drawable
    #   is an array of Gosu::Image. If not, this parameter is ignored. By
    #   default (when index is +Nil+), all the drawables are drawn.
    # @todo Remove the +if @drawable.is_a?(Text)+ line when text.rb will be removed
    def draw(color: @color, index: nil)
      if @drawable.is_a?(Text)
        @drawable.draw(@x, @y, @z, color: color)
      elsif @drawable.is_a?(Array)
        if index.nil?
          @drawable.each { |drbl| drbl.draw(@x, @y, @z, 1, 1, color) }
        else
          @drawable[index].draw(@x, @y, @z, 1, 1, color)
        end
      else
        @drawable.draw(@x, @y, @z, 1, 1, color)
      end
    end

    def hover?
      Window.instance.mouse_x.to_i >= @x - @hitbox &&
        Window.instance.mouse_x.to_i <= @x + @width + @hitbox &&
        Window.instance.mouse_y.to_i >= @y - @hitbox &&
        Window.instance.mouse_y.to_i <= @y + @height + @hitbox
    end

    def clicked?
      return false if Farewell::State.active_state.l_clicked.empty?

      Farewell::State.active_state.l_clicked[0] >= @x - @hitbox &&
        Farewell::State.active_state.l_clicked[0] <= @x + @width + @hitbox &&
        Farewell::State.active_state.l_clicked[1] >= @y - @hitbox &&
        Farewell::State.active_state.l_clicked[1] <= @y + @height + @hitbox
    end

    def move(x, y)
      @x += x
      @y += y
    end

    def move_to(x, y)
      @x = x
      @y = y
    end

    def add_event(event)
      type_err_msg = "expected a Farewell::Event, got #{event.class.name}"
      unless event.is_a?(Farewell::Event) || event.nil?
        raise TypeError, type_err_msg
      end

      @events.push(event)
    end

    def trigger_events(trigger)
      @events&.each { |event| event.trigger(trigger, self) }
    end

    # ==============================
    # Setters
    # ==============================

    def drawable=(drawable)
      # TODO: should I check every items of a drawable Array to be sure that
      #   they are a Gosu::Image ?
      @drawable = Gosu::Image.new(drawable) && return if drawable.is_a?(String)

      type_err_msg = "expected a Gosu::Image, an Array, a String or nil, got " +
        drawable.class.name.to_s
      unless drawable.is_a?(Gosu::Image) || drawable.is_a?(Array) ||
          drawable.nil?
        raise TypeError, type_err_msg
      end

      # todo: do not change the width and height here
      @drawable = drawable
      @width = drawable.width
      @height = drawable.height
    end

    def width=(width)
      @width = Integer(width)
    end

    def height=(height)
      @height = Integer(height)
    end

    def x=(x)
      @x = Integer(x)
    end

    def y=(y)
      @y = Integer(y)
    end

    def z=(z)
      @z = Integer(z)
    end

    def tag=(tag)
      @tag = Symbol(tag)
    end

    def hitbox=(hitbox)
      @hitbox = Integer(hitbox)
    end

    # @todo Test if Gosu::Color
    def color=(color)
      @color = color
    end
  end
end
