# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

module Farewell
  # The song [0][0] in @songs array is always the one playing. When calling
  # #play_next, the first song is removed, the indexes of the next songs are
  # downed and the new [0][0] song is played.
  class Playlist
    include Singleton
    attr_reader :playing, :songs

    def initialize
      @songs = []
    end

    def add(song, is_loop)
      #if (song.class == Gosu::Song)
      @songs << [song, is_loop]
    end

    def play
      return if @songs.empty?

      @songs[0][0].play(@songs[0][1]) unless @songs.empty?
      @playing = true
    end

    def pause
      return if @songs.empty?

      @songs[0][0].pause
      @playing = false
    end

    def clear
      return if @songs.empty?

      @songs[0][0].stop
      @songs.clear
      @playing = false
    end

    def empty?
      @songs.empty?
    end

    # Removes the first song of the list and play the next one
    def play_next
      @songs.shift
      play
    end

    def volume
      return if @songs.empty?

      @songs[0][0].volume
    end

    def volume=(float)
      return if @songs.empty?

      @songs[0][0].volume = float
    end
  end
end
