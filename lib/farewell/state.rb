# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

module Farewell
  class State
    attr_reader :entity_list, :l_clicked
    @@active_state = nil
    button_event = Farewell::Event.new
    button_event.on_hover { |entity| entity.draw(color: Gosu::Color::AQUA) }
    @@base_events = {button: button_event}
    @@base_entities = {}
    @@previous_ms = 0

    def initialize
      @entity_list = []
      @event_list = []
      @l_clicked = []
    end

    class << self
      def switch(new_state)
        puts "Switching state to: " + new_state.class.to_s
        @@active_state = new_state
        new_state.enter
      end

      def active_state
        @@active_state
      end
    end

    def enter
      @delta_ms = 0
      @fps_counter = Farewell::Entity.new(
        Text.new(Gosu.fps.to_s),
        Window.instance.width - 20,
        Window.instance.height - 20, 2
      )
    end

    def leave
    end

    def needs_cursor?
      # TODO: if true then do not display the custom cursor
      Config::NATIVE_CURSOR
    end

    def needs_redraw?
      true
    end

    def draw
      current_ms = Gosu.milliseconds
      @delta_ms = Gosu.milliseconds - @@previous_ms
      @@previous_ms = current_ms
      @fps_counter.drawable = Text.new(Gosu.fps.to_s)
      @fps_counter.draw

      # Triggering all on_hover events in the state
      Farewell::Entity.hover_list.each do |entity|
        entity.trigger_events(:hover)
      end
    end

    def update
      # The next song is automatically played if the previous one has ended.
      # Keep in mind that if the song is a loop, it never ends automatically.
      # Paused songs aren't considered as ended.
      if Gosu::Song.current_song.nil? && !Farewell::Playlist.instance.empty? &&
          Farewell::Playlist.instance.playing
        Farewell::Playlist.instance.play_next
      end
      Global::CLIENT.update
    end

    def button_down(id)
    end

    def button_up(id)
      @l_clicked = []
      if id == Gosu::MS_LEFT
        @l_clicked = [
          Window.instance.mouse_x.to_i,
          Window.instance.mouse_y.to_i,
        ]
        Farewell::State.active_state.entity_list.each do |entity|
          entity.trigger_events(:click) if entity.clicked?
        end
      end
    end

    def add_entity(entity)
      @entity_list.push(entity)
    end
  end
end
