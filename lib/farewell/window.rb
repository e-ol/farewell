# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# TODO: check for changes in Window and add in Farewell module before 0.1,
#   remove singleton
class Window < Gosu::Window
  include Singleton
  attr_reader :tileset_list
  attr_accessor :playing, :fullscreen, :l_clicked

  def initialize
    super 1920, 1080
    self.caption = 'Farewell'
    @tileset_list = {
      tileset: Gosu::Image.load_tiles('data/images/tileset.png', 16, 16, tileable: true),
      font1: Gosu::Image.load_tiles('data/images/font_tileset.png', 16, 16, tileable: true),
      font2: Gosu::Image.load_tiles('data/images/font_tileset2.png', 14, 18, tileable: true)
    }
    @pointer = Gosu::Image.new('data/images/cursor_v2.png')
    @playing = false
    @music_fade = false
    @update_interval = 33.33
  end

  def update
    if @music && false
      if @music_fade
        @music.volume -= 0.02
      end
      if !(@music.volume > 0) && @music_fade
        @music = @next_music
        @music_fade = false
        @music.play
        @music.volume = 1
      end
    end
    Farewell::State.active_state.update
  end

  def needs_cursor?
    Farewell::State.active_state.needs_cursor?
  end

  def needs_redraw?
    !@scene_ready || Farewell::State.active_state.needs_redraw?
  end

  def draw
    @scene_ready = true
    @pointer.draw(Window.instance.mouse_x, Window.instance.mouse_y, 10, 0.5, 0.5)
    Farewell::State.active_state.draw
  end

  def button_down(id)
    super
    Farewell::State.active_state.button_down(id)
  end

  def button_up(id)
    super
    Farewell::State.active_state.button_up(id)
  end

  def new_text_input
    @text_input = Gosu::TextInput.new
  end

  #def text_input
  #  super
  #end

  def music_switch(music)
    if @music
      @music_fade = true
      @next_music = music
    else
      @music = music
      @music.play(true)
    end
  end

  def close
    # TODO
    puts 'Closing'
    Session.instance.close
    close!
  end
end
