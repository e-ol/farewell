# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# A character from the local sqlite database.
class Character
  # Each character can practice one job from several; the job choosen by a
  # player is stored in this attribute.
  # @return [Job] The job of the character
  attr_accessor :current_job
  attr_reader :id, :name, :nickname, :gender, :life_count
  # The list of characters. This list is loaded once from the database.
  # @return [Array<Character>] All characters
  @@list = []

  def initialize(id, name, nickname, gender, life_count)
    @id = id
    @name = name
    @nickname = nickname
    @life_count = life_count
    @gender = gender
  end

  def jobs
    return if @id.nil?

    retval = []
    db = SQLite3::Database.new(Global::DATABASE)
    db.execute("SELECT * FROM job JOIN practice ON job.id = practice.job_id " \
      "WHERE character_id = ?", @id) do |row|
      retval << Job.new(row[0], row[1], row[2], row[3])
    end
    retval
  end

  def to_h
  end

  def to_json(*options)
    to_h.to_json(*options)
  end

  class << self
    def list
      return @@list unless @@list.empty? || @@list.nil?

      @@list = load_list
    end

    def get_by_id(id)
      list.each { |character| return character if character.id == id }
      nil
    end

    # Search for a nickname in the character list and return the corresponding
    #   +Character+
    #
    # @param nick [String] The nickname to search. Case insensitive
    def get_by_nick(nick)
      list.each { |character| return character if character.nickname.downcase == nick.downcase }
      nil
    end

    # Search for nicknames and return the characters
    #
    # @param nicks [Array<String>] The nicknames array
    # @return [Array<Character, nil>] The corresponding characters. If a
    #   character can't be found, its value will be nil.
    # @see Character.get_by_nick
    def get_by_nicks(nicks)
      retval = []
      nicks.each do |nick|
        retval << get_by_nick(nick)
      end
      retval
    end

    private

    # @todo try to find database, check if any row
    def load_list
      retval = []
      db = SQLite3::Database.new(Global::DATABASE)
      db.execute("SELECT * FROM character;") do |row|
        gender =
          case row[3]
          when "F"
            :female
          when "M"
            :male
          else
            :neutral
          end
        retval << Character.new(row[0], row[1], row[2], gender, row[4])
      end
      retval
    end
  end
end

# A job tells what actions the character can perform, what skills he knows,
# whats goals he can have and what items he begin with
# @!attribute [rw] id
#   The id of the job in the database. Useful for the server
# @!attribute [rw] f_name
#   Name of the job, female gendered
# @!attribute [rw] m_name
#   Name of the job, male gendered
# @!attribute [rw] description
#   The description of the job
Job = Struct.new(:id, :f_name, :m_name, :description)

# A goal is a directive that brings fame points to the player. Some gives items
# and/or special actions. Most of the goals should be kept secret by the player.
# The logics of the goals are coded in the server.
# @!attribute [rw] id
#   The id of the goal in the database. Useful for the server.
# @!attribute [rw] name
#   The name of the goal.
# @!attribute [rw] description
#   A short description of how to reach the goal.
Goal = Struct.new(:id, :name, :description)
