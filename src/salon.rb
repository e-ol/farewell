# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

class Salon
  attr_accessor :player_list, :player_count

  def initialize(player_count = 0)
    @player_count = player_count
    @player_list  = []
  end

  class << self
    def from_hash(hash)
      salon = Salon.new(hash[:player_count])
      hash[:players].each do |user|
        salon.player_list.push(User.new(user[:User][:login]))
      end
      salon
    end
  end
end
