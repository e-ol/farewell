# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

class Session
  include Singleton

  attr_reader :user, :salon, :character

  def initialize
  end

  def user=(user)
    @user = user
  end

  def salon=(salon)
    @salon = salon
  end

  def character=(character)
    @character = character
  end

  def close
    @user&.disconnect
  end
end
