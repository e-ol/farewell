# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# TODO: redo the textinput's behavior
class ConnectionState < Farewell::State
  include Singleton

  def initialize
    super
    @blink = 0
    @char_count = 0
    @char_position = 0
    @carriage_return_count = 0
  end

  def enter
    super
    Global::CLIENT.connect
    @login_text = Farewell::Entity.new(Text.new(t(:t_login, scope: :connection)), 0, 64, 2, h_align: :center)
    @underscore = Farewell::Entity.new(Text.new('_'), -128, 90, 2, h_align: :center)
    @login_input = Farewell::Entity.new(Text.new(''), -128, 90, 2, h_align: :center)
    @password_input = Farewell::Entity.new(Text.new(''), -128, 106, 2, h_align: :center)
    Window.instance.text_input = Gosu::TextInput.new
  end

  def leave
    super
  end

  def needs_redraw?
    super
  end

  def update
    super
    connection_status = Global::CLIENT.get_pending('connection')
    unless connection_status.nil?
      if connection_status['status'] == 'success'
        Session.instance.user = User.new(@user.login, connection_status["token"])
        Farewell::State.switch(HomeState.instance)
      end
    end
    @blink += 1
    @blink = 0 if @blink > 60
    @char_count = Window.instance.text_input.text.length
    if @char_count != @char_position
      case @carriage_return_count
      when 0
        @login_input.drawable = Text.new(Window.instance.text_input.text)
        # TODO: Once todo#1 is done in Farewell::Entity, use Farewell::Entity width instead of its drawable below
        @underscore.x = Window.instance.width / 2 - 128 + @login_input.drawable.width
      when 1
        @password_input.drawable = Text.new(Window.instance.text_input.text)
        # TODO: Once todo#1 is done in Farewell::Entity, use Farewell::Entity width instead of its drawable below
        @underscore.x = Window.instance.width / 2 - 128 + @password_input.drawable.width
      end
      @char_position = @char_count
    end
  end

  def draw
    super
    @login_text.draw
    @underscore.draw if @blink < 30
    @login_input.draw
    @password_input.draw
  end

  def button_down(id)
    super
    case id
    # when Gosu::MS_LEFT

    when Gosu::KB_ESCAPE
      Farewell::State.switch(SettingsState.instance)
    when 40, Gosu::KB_ENTER
      case @carriage_return_count
      when 0
        @underscore.x = Window.instance.width / 2 - 128
        @underscore.y = 106
        @user = User.new(Window.instance.text_input.text)
        Window.instance.text_input.text = ''
        @char_position = 0
        @carriage_return_count = 1
      when 1
        @user.password = Window.instance.text_input.text
        @user.connect
      end
    end
  end
end
