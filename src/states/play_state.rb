# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

class PlayState < Farewell::State
  include Singleton

  def initialize
    super
  end

  def enter
    super
    font_height = 28
    dialog_box = Farewell::Entity.new(Gosu::Image.new("data/images/dialog-box.png"),
      0, 0, 0, h_align: :right)
    # todo
    history = Farewell::Entity.new(Gosu::Image.from_text("", font_height),
      0, dialog_box.height + 5, 0, width: dialog_box.width - 18, h_align: :right)
    @chat = Chat.new(dialog_box, history, Gosu::Image.from_text("_", font_height),
      font_height, :downward)
  end

  def update
    super
    @chat.update
  end

  def draw
    @chat.draw
    super
  end

  def button_down(id)
    super
    if [Gosu::KB_RETURN, Gosu::KB_ENTER].include?(id)
      # TODO: manage maj + (return | enter)
      if Window.instance.text_input
        @chat.send
      else
        @chat.dialog_box.trigger_events(:click)
      end
    end
  end
end
