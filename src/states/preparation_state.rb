# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

class PreparationState < Farewell::State
  include Singleton

  def initialize
    super
    @fade_out_volume = 0.02
    @previous_ms = 0
  end

  def enter
    super
    @loading_index = 0
    @has_choose = false
    @fading_out_song = true
    # List of Hashes containing each job choice.
    # Keys of these job choices are :icon, :name, :description
    @choice_list = []
    @waiting_text = Farewell::Entity.new(
      Gosu::Image.from_text("Waiting for players to choose", 22), 0, 6, 0,
      h_align: :center
    )
    @@base_entities[:loading].move_to(Window.instance.width / 2 -
      (@@base_entities[:loading].width / 2), 45)
    @previous_ms = 0
    set_character
  end

  def update
    super
    status = Global::CLIENT.get_pending("update")

    if !status.nil? && status["status"] == "started"
      Farewell::State.switch(PlayState.instance)
    end

    if @fading_out_song
      if Farewell::Playlist.instance.volume > 0.01
        Farewell::Playlist.instance.volume -= @fade_out_volume
      else
        @fading_out_song = false
        Farewell::Playlist.instance.clear
        Farewell::Playlist.instance.add(Gosu::Song.new("data/music/Larik.ogg"),
          false)
        Farewell::Playlist.instance.play
      end
    end
  end

  def draw
    super
    # TODO: if character not received, print a loading message
    return unless Session.instance.character

    if !@has_choose
      draw_choose_job
    else
      draw_wait_for_players
    end
  end

  def button_down(id)
    super
  end

  def draw_choose_job
    @choice_list.each { |choice_box| choice_box.each_value(&:draw) }
  end

  def draw_wait_for_players
    @waiting_text.draw
    if @loading_index.to_i == @@base_entities[:loading].drawable.length - 1
      @loading_index = 0
    else
      @loading_index += @delta_ms * 0.03
    end
    @@base_entities[:loading].draw(index: @loading_index.to_i)
  end

  def set_character
    # todo: check if any character, else print error
    character = Session.instance.character
    choice_box_y = 20
    choice_box_x = -10
    line_height = 22
    character.jobs.each do |job|
      event = Farewell::Event.new
      event.on_click { choose_job(job) }
      choice_box = {}
      choice_box[:name] =
        if character.gender == :female
          Farewell::Entity.new(
            Gosu::Image.from_text(job.f_name, line_height), choice_box_x,
            choice_box_y, h_align: :right
          )
        else
          Farewell::Entity.new(
            Gosu::Image.from_text(job.m_name, line_height), choice_box_x,
            choice_box_y, h_align: :right
          )
        end
      choice_box[:description] = Farewell::Entity.new(
        Gosu::Image.from_text(job.description, line_height, width: 800,
                                                            align: :right),
        choice_box_x, choice_box_y + (line_height * 2), h_align: :right
      )
      # TODO: instead of that, create an entity "outline" in which to attach the
      choice_box[:name].add_event(event)
      choice_box[:description].add_event(event)
      @choice_list << choice_box
      choice_box_y += line_height * 7
    end
  end

  # @todo An option to rollback the choice
  def choose_job(job)
    @has_choose = true
    Session.instance.character.current_job = job
    Global::CLIENT.send({setting: {
      set: "job",
      value: Session.instance.character.current_job.id,
      token: Session.instance.user.token,
    }}.to_json)
  end
end
