# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

class StartState < Farewell::State
  include Singleton

  def initialize
    super
    @@fonts = {} unless defined?(@@fonts)
    @@fonts["bluunext"] = "data/fonts/BluuNext/BluuNext-Bold.otf"
  end

  def enter
    super
    loading_tiles = Gosu::Image.load_tiles("data/images/loading-firefly.png", 32, 64)
    @@base_entities[:loading] = Farewell::Entity.new(loading_tiles)
    Farewell::Playlist.instance.clear
    Farewell::Playlist.instance.add(Gosu::Song.new("data/music/The_dance_of_death_part1.wav"), false)
    Farewell::Playlist.instance.add(Gosu::Song.new("data/music/The_dance_of_death_loop1.wav"), true)
    Farewell::Playlist.instance.play
    @farewell_text = Farewell::Entity.new(
      Gosu::Image.from_text("Boundary Madness", 80, font: @@fonts["bluunext"]),
      20, 4, 2
    )
    @play_menu = Farewell::Entity.new(
      Gosu::Image.from_text(t(:b_play, scope: :start), 80, font: @@fonts["bluunext"]),
      90, 120, 2
    )
    @arrow = Farewell::Entity.new(
      Farewell::Drawable.new(Window.instance.tileset_list[:font1][47]), 19, 32, 2
    )
    @play_menu.add_event(@@base_events[:button])
  end

  def leave
    super
  end

  def needs_redraw?
    super
  end

  def update
    super
  end

  def draw
    @farewell_text.draw(color: Gosu::Color.new(Color::PRIMARY))
    @play_menu.draw
    # if @play_menu.hover?
    #  @arrow.draw
    # end
    # @settings_menu.draw
    # @compendium_menu.draw
    # @quit_menu.draw
    super
  end

  def button_down(id)
    super
  end

  def button_up(id)
    super
    Farewell::State.switch(ConnectionState.instance) if @play_menu.clicked?
    Farewell::State.switch(SettingsState.instance) if false
    Window.instance.close if false
  end
end
