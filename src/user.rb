# frozen_string_literal: true

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

class User
  attr_accessor :login, :status
  attr_reader :token

  def initialize(login, token = nil)
    @login = login
    @token = token if token
  end

  # TODO: This must encrypt password before saving it
  def password=(password)
    @password = password
  end

  def connect
    Global::CLIENT.send("{\"connection\":#{to_json}}")
  end

  def disconnect
    Global::CLIENT.send("{\"disconnection\":#{to_json}}")
  end

  def join_salon
    if @token
      Global::CLIENT.send({salon: {asks_to: 'join', token: @token}}.to_json)
    else
      puts 'Trying to join a salon without token'
    end
  end

  def leave_salon
    Global::CLIENT.send({salon: {asks_to: 'leave', token: @token}}.to_json)
  end

  def to_h
    hash = {login: @login}
    hash[:password] = @password if @password
    hash[:token] = @token if @token
    hash
  end

  def to_json(*options)
    to_h.to_json(*options)
  end

  def connected?
    # TODO: replace login by id token
    @login == Session.instance.user.login && !Session.instance.user.login.nil?
  end
end
